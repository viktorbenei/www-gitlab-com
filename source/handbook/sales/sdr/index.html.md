---
layout: markdown_page
title: "Sales Development"
---

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Job Description

As a Sales Development Representative (SDR) you are focused on driving net new business for the company. We thrive on competition and our ability to cold call/email. You will be aligned with Account Executives (AE’s) to work a strategic list of 50-100 targeted accounts on a quarterly basis..

## Expectations

You will be expected to:
* Build a trusting relationship with your assigned AE’s
* Hit your monthly goals. Which are opportunities/SQLs (Sales Qualified Leads) generated
* Have a good balance of phone calls made and emails sent (there are no daily metric requirements around this)
* Complete the GitLab beginner, intermediate, and expert certification courses
* Be able to demo GitLab to one of the sales directors

## SDR Onboarding 

To prepare you to be a successful SDR at GitLab we have created a 4 week bootcamp (link to sales bootcamp url). At the end of 4 weeks, success is defined as preparing you to have conversations with prospects and create sales opportunities.
Each week there will be a defined goal of what you should learn and be able to apply with information and training to support the learning objective.

### [SDR Master Onboarding](https://docs.google.com/document/d/1ieddQChPZplU7toak2mZsyB3MiXQkbiwkJ24zS3Fjxo/edit)
### [SDR Playbook Onboarding](https://about.gitlab.com/handbook/sales/sdr/sdr-playbook-onboarding/)

## Working with AE’s

A good portions  of your job requires you to work with assigned AE’s. This is key to our role as SDRs’ because they will not only help you create an appropriate strategy to go after specific accounts, but they will help you build your career as a future AE for GitLab. It is crucial that you build a relationship of honesty and trust. 

### Account Distribution

* When you are assigned to your AE’s you will work with them to create a list of accounts for you to go after. This should consist of ~50-100 Large or Strategic accounts to work in a given quarter. 
* In the situation that you do not have assigned AE’s, your Team Lead will provide you with a list of unnamed top strategic or large accounts to go after. (When you create meetings you will need to coordinate with your Team Lead to decide what AE that account will go to. You will also work with your Team Lead to develop strategy for your assigned accounts).
* In the event that you feel your accounts are non-workable please consult with your Team Lead.
* We use this [report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of stategic and large accounts and effective use of SDR resources.  

### Account Management

* We use SalesForce as our CRM to manage accounts. 
* When it comes to task management there are a couple of different ways to do so. You can use either SFDC (SalesForce.com) or you can use Outreach which is primarily used for emailing. 

### SDR/AE Relationship

* Communication is priority in our company. You will need to create a working relationship with your assigned AE’s. 
* The relationship between you and your AE’s should be one of trust, honesty, and openness. It is important to understand that this relationship is a two-way street. The SDR and the AE will need to put in equal amounts of effort to make this relationship work. When the relationship works, both the SDR and AE become successful. 
* There should be, at minimum, a weekly 1:1 with each your AE’s to discuss:
* Where you are at for the month, metric wise.
* What you have done with the accounts you are working. (This will be what is working well, what you have found out about company structure, who are the right people to talk to, and meetings/opportunities you have created.)
* Discuss evolving strategy for “currently working” accounts and “new accounts” you will be going after. 

## Metrics

* SDR’s compensation is based on 1 key metrics:
   * Opportunities created (SQL’s)

* SDRs’ will also be measured on, but not tied to their compensation:
    * Number of contacts added to named account
    * Account Mapping
    * Roles identified for each contact
    * Software development tools being used
    * How the organization is set up - who are the other teams that we can expand into
    * Purchasing process - do they have a procurement team, do/can they use a fulfillment agency
    * % of named accounts contacted
    * Number of contacts with activity (email and/or calls)
    * Number of emails sent
    * Number of calls made
    * Number of connects (calls between SDR and prospect)
    * Number of sales appointments (calls between prospect, AE and SDR to explore if there is an opportunity)
    * Pipeline of SDR sourced opportunities
    * ACV won from opportunities SDR sources 

## SDR Daily Stand-up Meeting

The purpose of the SDR daily stand-up meeting is to set daily goals for ourselves and to review what we did the day before. This helps each of us individually to make sure we are on track to hit our monthly numbers. Also, it is a good time for us to reflect on what is working well and what we are struggling with when it comes to our goals we set each day.

## SDR Weekly Team Meeting

The purpose of the weekly SDR team meeting is to go over important changes that are happening with the team, update on monthly metrics, open discussion on any questions people have, hiring updates, etc. It is important for us to meet each week as a team just to see each other and to be able to talk with each other as a group.

## Criteria for an SQL
 
 1. Authority 
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.)
  * OR is this someone that has a pain and a clear path to the decision maker

 2. Need
  * The buyer has either a defined project or a pain has been identified
  * If organization already has an open opportunity or is a customer, SQL needs to be for a different group, buying process and budget.
  
 3. Required Information
  * Number of potential EE users on this opp
  * Current technologies and when the contract is up
  * Current business initiatives
  * When naming opportunity, make sure to add in division or group to the opportunity name

 4. Handoff
  * The buyer is willing to meet with an AE

## Criteria for Sales Accepted Opportunity (SAO)

 1. SQL Definition Met
  * AE confirmed authority and need
  * AE confirms number of potential EE users
  * AE confirms that the SQL information is logged into SFDC

 2. Handoff
  * The SDR has made an introduction with the AE to the prospect via email
  * SDR schedules a calendar invite with buyer and AE
  * The SDR confirms the agenda and set expections for the phone call

 3. Meeting Completed
  * The AE conducts the first meeting via phone (it is recommended that the SDR is on the phone call as well)
  * The AE re-confirms the information passed by the SDR

 4. Next Step Identified
  * The AE schedules a next event with the AE (typically another phone call to dive in deeper to the clients pains and needs)
  * The AE has 1 business day to accept/reject the opportunity

## Ramping for new hires

**If you started carrying a quota at the beginning of the month:**

Month 1:
 
 * Daily/Weekly/Monthly Activity [40/150/600]
 * Daily/Weekly/Monthly Emails [20/100/400]
 * Daily/Weekly/Monthly Calls [10/50/200]
 * % of named accounts contacted (monthly) [25%]
 * Average # of contacts per named account contacted (monthly) [5]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [5/15]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [8]
 * % of named accounts with sales appointment [10%]
 * Opportunities created (weekly/monthly) [0.75/3]
 * * % of named accounts with an opportunity [10%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 2:
 
 * Daily/Weekly/Monthly Activity [60/300/1200]
 * Daily/Weekly/Monthly Emails [40/200/800]
 * Daily/Weekly/Monthly Calls [20/100/400]
 * % of named accounts contacted (monthly) [50%]
 * Average # of contacts per named account contacted (monthly) [10]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [3]
 * Connects (weekly/monthly) - calls SDR’s are having [10/40]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3/12]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [1.25/5] 
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0] 

Month 3+:

 * Daily/Weekly/Monthly Activity [90/450/1800]
 * Daily/Weekly/Monthly Emails [60/300/1200]
 * Daily/Weekly/Monthly Calls [30/150/600]
 * % of named accounts contacted (monthly) [75%]
 * Average # of contacts per named account contacted (monthly) [15]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [15/60]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [5/20]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [2/8]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly) [$80,000] 

**If you started carrying a quota halfway through the month:**

Month 1:
 
 * Daily/Weekly/Monthly Activity [40/150/300]
 * Daily/Weekly/Monthly Emails [20/100/400]
 * Daily/Weekly/Monthly Calls [10/50/200]
 * % of named accounts contacted (monthly) [25%]
 * Average # of contacts per named account contacted (monthly) [5]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [5/15]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [6]
 * % of named accounts with sales appointment [10%]
 * Opportunities created (weekly/monthly) [1/2]
 * * % of named accounts with an opportunity [10%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 2:
 
 * Daily/Weekly/Monthly Activity [60/300/1200]
 * Daily/Weekly/Monthly Emails [40/200/800]
 * Daily/Weekly/Monthly Calls [20/100/400]
 * % of named accounts contacted (monthly) [50%]
 * Average # of contacts per named account contacted (monthly) [10]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [3]
 * Connects (weekly/monthly) - calls SDR’s are having [10/40]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3/12]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [1.25/5] 
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly/quarterly) [$0] 

Month 3+:

 * Daily/Weekly/Monthly Activity [90/450/1800]
 * Daily/Weekly/Monthly Emails [60/300/1200]
 * Daily/Weekly/Monthly Calls [30/150/600]
 * % of named accounts contacted (monthly) [75%]
 * Average # of contacts per named account contacted (monthly) [15]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [15/60]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [5/20]
 * % of named accounts with sales appointment [33%]
 * Opportunities created (weekly/monthly) [2/8]
 * % of named accounts with an opportunity [33%]
 * ACV won from sourced opportunities (monthly) [$80,000] 

### Salesforce Hygiene for your opportunites 

[SDR sourced opps](https://na34.salesforce.com/00O61000003iXGw) This is the report that the leadership uses to see all the opps that have been created from the SDR team. I strongly recommend that you live in this report for the opps that you are creating. If you make sure that all the above information is as accurate as possible it will help you to not be under the microscope from your manager and the executive team. 

I highly recommend also to stay on top of your AE’s to make sure they are progressing the opportunity from BDR qualified to Discovery stage to make sure you get paid on the opps that you are creating. All of the above steps and processes are only going to help you create healthy habits for when you become an AE. 

It will be in your best interest to also sit in on as many meetings as possible with your different AE’s and at different stages in the buying process to see how the AE’s work with prospects beyond qualifying. The idea is to create a habits for success. 
