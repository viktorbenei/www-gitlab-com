## Salesforce

### Standard Object Definitions

#### Lead

A lead is a prospect who has yet to be qualified. Lead may have expressed interest by requesting contact, starting a trial, attending a webinar or trade show, or registering on GitLab.com or our customer portal. Leads are typically followed up by our Business Development Reps (BDR) or Account Executives. During this initial qualification process, a BDR will uncover certain details that will determine whether the prospect is qualified. For more information on the BDR Qualification process, [click here](https://about.gitlab.com/handbook/marketing/business-development/#qualifying). Once qualified, the lead is converted into an Account, Contact, and Opportunity.

#### Account

An account can either be a prospect who was qualified by a BDR or Account Executive, an existing customer, or a partner. Accounts will contain all company information, including billing and shipping address, company phone, website, support package, industry, employee count, and annual revenue. An account will be the parent record for associated contacts, opportunities, subscriptions, invoices, and payments.

#### Contact

A contact is a person associated to an account record. Information contained in the contact record include name, mailing address, title, role, phone, and email. An account can have many contacts associated to it. Contacts can be added to opportunities via [Contact Roles](https://help.salesforce.com/articleView?id=000005632&type=1&language=en_US).

#### Opportunity

An opportunity is created when a prospect has expressed interest in GitLab. The opportunity record includes information regarding close dates, contract value, and type. An opportunity will be in a certain stage, depending on where it is in the sales cycle. For more information on stages, click [here](https://about.gitlab.com/handbook/sales/#opportunity-stages).

#### Tasks

#### Dashboards

#### Reports



### External Resources
* [Salesforce Success Community](https://success.salesforce.com/)
* [Salesforce Help](https://help.salesforce.com/home)
* [Salesforce Glossary](https://help.salesforce.com/articleView?id=glossary.htm&type=0)

#### Commonly Asked Questions
* [How to Create a Report](https://help.salesforce.com/articleView?id=reports_builder_create.htm&type=0)
* [How to Create a List View](https://help.salesforce.com/articleView?id=customviews.htm&type=0&language=en_US&release=208.6)