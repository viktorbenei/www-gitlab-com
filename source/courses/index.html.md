---
layout: markdown_page
title: "Courses"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Introduction
{: #introduction}

Welcome to the courses page! This is the central location to find course information, listings and links to content. If you are coming here to add a course please follow the instructions below.

## Adding a Course
{: #adding-a-course}

If you have anything to share (no matter the quality level) please add it to this page by:

1. Making sure all the content is publicly viewable. Upload video's to our YouTube channel. If there is a presentation in Google Sheets make sure anyone can view it. If there is written content either add it to the relevant part of the handbook or create a page like https://about.gitlab.com/handbook/people-operations/courses/sls-101 and assign the merge request to Abby Matthews who will happily review and merge it for you.
1. Give the code a unique identifier in the form of AAA111, first three letters are for the department, numbers are unique and first number specifies the difficulty level of the course. As a guideline, in many university settings, there are only two levels, i.e. 1xx for "introductory" courses, and 2xx for more advanced courses.
1. Add the course to the bottom of this page. If you made a course list on another page (like university or support) you can use just one link to link to the entire list. If the courses are not in one list please link to each individually.
1. Optionally you can create a quiz in Grovo.

Notes:

- We do not create custom course content in Grovo because everyone should be able to contribute to the courses. The courses are part of our handbook or documentation and versioned with git so people can contribute via merge requests. The exception to this are the the individual (IC) and manager (MGR) courses that consist of standard Grovo content.
- All videos are publicly listed on Youtube under our account so they are easy to discover and accessible from many different platforms.

## Getting Access to Youtube
{: #getting-access-to-youtube}

1. If you don't have access to the Youtube channel you can find the details in 1password in the Team Vault.
1. Type in Youtube, you should see a secure note with the information on how to get added to the account.

## Uploading Courses to Youtube
{: #uploading-courses-to-youtube}

TODO

## Course Listings
{: #course-listings}

### Individual Contributor (IC) courses

- IC 004 Social Media (28 mins):In today's connected world social media is becoming an essential tool for generating business, responding to customers and sharing content. This course provides some hints, advice and guidance on how to use social media responsibly.
- IC 120 Building Effective Communication Skills (13 mins):Communication is a blend of art and science.  To be effective communicators, we must be diligent about practicing and improving our skills.
- IC 130 Collaboration & Consensus (8 mins):Collaboration and consensus are effective ways to work together as a team towards a common goal. Find out more about these approaches in this course.
- IC 140 Productivity Under Pressure (10 mins):Explore ways to effectively manage your workload when the pressure is really on.
- IC 141 Effective Productivity (30 mins):Discover how to schedule time efficiently, prioritize effectively and improve concentration so that your productively is maximized.
- IC 143 How to Manage Projects (36 mins): This course will give an overview of the general process of managing projects. This includes defining, scoping and identifying project tasks. The second half of the course will cover how to include others in your project plan.

### Manager (MGR) courses

- MGR 100 The Role of a Manager (15 mins): Taking the leap from individual contributor to manager is great for your career, but it will also introduce challenges you haven't dealt with before.  Learn how to effectively navigate in the role of a manager.
- MGR 101 Develop yourself as a Manager (1hr): This course has five chapters covering; management styles, decision making approaches, data driven management, being human and professional development. This is to provide you with a fuller perspective on how you as a manager can really develop yourself and your role.
- MGR 120 [IC Communicating Effectively](#for-individual-contributors) (18 mins)
- MGR 140 Productivity Under Pressure (12 mins)
- MGR 160 Managing Performance Issues (12 mins): Identifying and addressing performance issues early will impact positively on a team's moral, engagement and ability to achieve results while also reducing turnover.  Managers who establish and expect accountability will develop stronger individual contributors and earn the respect of their team.
- MGR 161 Develop Your Team (31 mins): Your team members are all different, which is part of what makes them unique. Before you get the best out them you need to first take some time and understand their working styles, strengths and weaknesses. A crucial part of a manager's role is to give feedback. In addition, you need to know when to give it and the types of approaches to feedback so that your team members respond in a positive way.
- MGR 162 Motivate & Enable Your Team (43 mins): Learn how to motivate your team and understand what it means to be engaged. Discover if you have a strong team commitment and what motivates your team to invest in their work.
- MGR 165 Self Improvement & Team Dynamics (31 mins): Understanding yourself, your emotions and reactions allows you to master them so you can better support your team. A crucial part of getting the most out of your team is by letting them know you can be trusted and they can trust each-other
- MGR 170 Financial Fundamentals (18 mins no quiz): Learn all about generating revenue and profit. Understand the foundations of financial information and the importance of budgeting
- MGR 200 Strategic Management (35 mins): Big picture thinking requires planning and understanding the business at all levels of management. Leading or directing across teams requires influence and excellent communication skills. You need to establish and leverage good networks to get the results and resources you need to make everyone succeed.
- MGR 210 Fostering Creativity & Innovation (44 mins): Discover tools and techniques that will encourage your team to bring their great ideas forward. Ask yourself what does it mean to be creative and how can you as leader use design thinking to encourage others in your team to do the same? The final chapter of this course is all about change, be ready to adapt to change by planning and communicating your vision. Here you will also learn how to measure and evaluate the effectiveness of change initiatives.

### University (UNI) courses

- TODO Code all courses on [https://docs.gitlab.com/ce/university/](https://docs.gitlab.com/ce/university/) and add a single link from here (instead of listing all courses which would lead to duplication).

### Sales (SLS) courses

- TODO

### Finance (FIN) courses

- TODO

### Build (BLD) courses

- BLD101 XYZ TODO

### Engineering (DEV) courses

- [DEV 101 - Contributing to Golang projects](/courses/dev-101)

### Backend (BCK) courses

 - TODO

### Infrastructure (INF) courses

- [INF 201 - Using Terraform to manage the GitLab.com infrastructure](/courses/inf-201)
