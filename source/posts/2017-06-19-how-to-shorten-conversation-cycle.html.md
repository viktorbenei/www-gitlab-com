---
title: How to Shorten the Conversation Cycle
author: Rebecca Dodd
author_twitter: 
author_gitlab: rebecca
categories: GitLab workflow
image_title: '/images/blogimages/shorten-conversation-cycle.jpg'
twitter_image: '/images/tweets/how-to-shorten-conversation-cycle.png'
description: Four simple steps to move faster from idea to production.
---

If your new features often get stalled in the initial discussion phase, read our four tips for shortening the conversation cycle and shipping faster.

<!-- more -->

## 1. Measure Your Cycle Time

The first step towards making a change is having the numbers to motivate it. If you measure the duration of time from the moment an idea is first discussed in chat, all the way through to its release in production, you can make a good case for changing your approach if others can see that something is causing delays. Try a feature like [cycle analytics](https://about.gitlab.com/features/cycle-analytics/) to monitor each stage in your workflow.

## 2. Start with Minimum Viable Changes

You've identified the problem, now how do you fix it? Where ideas for new features and improvements often get stuck is on how to implement them. The idea may be too ambitious or too time consuming to ship easily, so it gets pushed back in favor of more manageable changes. Try breaking up new products or features into smaller pieces of functionality. [Iteration is one of our company values](https://about.gitlab.com/handbook/values/) and while it's often one of the more uncomfortable ones, it is effective. Do the smallest thing possible and release it quickly – you can keep iterating from there.

## 3. Include Gatekeepers Early on

Who needs to approve something before you ship? Don't leave them out until the last minute. Including stakeholders, security experts, product managers and UX team members in the conversation in the early phases prevents bottlenecks ahead of release, and ensures that most errors have been caught and addressed before you move into production. Read more about [shipping faster without sacrificing security or quality](https://about.gitlab.com/2017/06/05/speed-security-quality-with-hackerone/).

## 4. Get Everyone on Board

Acknowledging that a feature or product is not polished and needs more work, yet releasing it anyway, feels unnatural to must of us, so you may meet some resistance to the idea. Working in this way does offer benefits to both business owners and developers, which you can communicate to help persuade hesitant team members.

For example, you can respond more quickly to market needs and user feedback by shipping minimum viable changes often, which is good news for your business. For developers, it's easier to troubleshoot a small release and having faster, more frequent feedback on work gives more of a sense of progress and boosts motivation.

Moving towards smaller releases to shorten the time between idea and production may feel strange at first, but you'll start seeing results quickly.
Shortening the conversation cycle is just one principle of Conversational Development. Visit [conversationaldevelopment.com](http://conversationaldevelopment.com/) to learn more.
{: .alert .alert-gitlab-orange}  

[Cover image](https://unsplash.com/@djmalecki?photo=fw7lR3ibfpU) by [Dawid Malecki](https://unsplash.com/@djmalecki) is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
{: .note}
